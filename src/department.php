<?php

namespace App;

use JMS\Serializer\Annotation as Serializer;

class Department implements \JsonSerializable
{
    /**
     * @var int $id
     * @Serializer\Type("int")
     */
    public $id;

    /**
     * @var string $name
     * @Serializer\Type("string")
     */
    public $name;

    public function jsonSerialize() :mixed
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}