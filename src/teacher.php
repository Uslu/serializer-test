<?php

namespace App;

use JMS\Serializer\Annotation as Serializer;

class Teacher extends Person implements \JsonSerializable
{

    /**
     * @var int $id
     * @Serializer\Type("int")
     */
    public $id;
    /**
     * @var int $salary
     * @Serializer\Type("int")
     */
    public $salary;
    /**
     * @var Lesson[] $lessons
     * @Serializer\Type("array<App\Lesson>")
     */
    public $lessons = [];
    /**
     * @var Department[] $department
     * @Serializer\Type("array<App\Department>")
     */
    public $departments = [];

    public function getLessons()
    {
        return $this->lessons;
    }

    public function getDepartments()
    {
        return $this->departments;
    }

    public function jsonSerialize() :mixed
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'age' => $this->age,
            'salary' => $this->salary,
            'lessons' => $this->lessons,
            'department' => $this->departments,
        ];
    }
}