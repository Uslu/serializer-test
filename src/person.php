<?php

namespace App;

use JMS\Serializer\Annotation as Serializer;

class Person implements \JsonSerializable
{

    /**
     * @var string $name
     * @Serializer\Type("string")
     */
    public $name;

    /**
     * @var int $age
     * @Serializer\Type("int")
     */
    public $age;

    /**
     * @var string $gender
     * @Serializer\Type("string")
     */
    public $gender;

    public function jsonSerialize() :mixed
    {
        return [
            'name' => $this->name,
            'age' => $this->age,
            'gender' => $this->gender
        ];
    }
}