<?php

namespace App;

use ISchool;
use JMS\Serializer\Annotation as Serializer;

class Student extends Person implements \JsonSerializable
{

    /**
     * @var int $id
     * @Serializer\Type("int")
     */
    public $id;

    /**
     * @var Department $department
     * @Serializer\Type("App\Department")
     */
    public $department;
    /**
     * @var Lesson[] $lessons
     * @Serializer\Type("array<App\Lesson>")
     */
    public $lessons = [];

    public function getDepartment()
    {
        return $this->department;
    }

    public function getLessons()
    {
        return $this->lessons;
    }

    public function jsonSerialize() :mixed
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'age' => $this->age,
            'department' => $this->department,
            'lessons' => $this->lessons,
        ];
    }
}