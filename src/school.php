<?php

namespace App;

use JMS\Serializer\Annotation as Serializer;

class School implements \JsonSerializable
{

    /**
     * @var Department[] $departments
     * @Serializer\Type("array<App\Department>")
     */
    public $departments = [];
    /**
     * @var Teacher[] $teachers
     * @Serializer\Type("array<App\Teacher>")
     */
    public $teachers = [];
    /**
     * @var Student[] $students
     * @Serializer\Type("array<App\Student>")
     */
    public $students = [];
    /**
     * @var Lesson[] $lessons
     * @Serializer\Type("array<App\Lesson>")
     */
    public $lessons = [];

    public function getDepartments()
    {
        return $this->departments;
    }

    public function getTeachers()
    {
        return $this->teachers;
    }

    public function getStudents()
    {
        return $this->students;
    }

    public function getLessons()
    {
        return $this->lessons;
    }

    public function jsonSerialize() :mixed
    {
        return [
            'departments' => $this->departments,
            'teachers' => $this->teachers,
            'students' => $this->students,
            'lessons' => $this->lessons,
        ];
    }
}