<?php

require __DIR__ . '/vendor/autoload.php';

$files = glob(__DIR__ . '/src/*.php');

foreach ($files as $file) {
    require($file);
}

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

use JMS\Serializer\SerializerBuilder;

use App\Student;
use App\Teacher;
use App\Lesson;
use App\Department;
use App\School;

class Index
{

    public $testTimes;

    public function __construct()
    {
        $this->testTimes = [
            "encode" => [
                "symfony" => 0,
                "jms" => 0,
                "php" => 0,
            ],
            "decode" => [
                "symfony" => 0,
                "jms" => 0,
                "php" => 0,
            ],
        ];
    }

    public function run()
    {

        $steps = 100;
        for ($i = 0; $i < $steps; $i++) {
            $this->test();
        }

        foreach ($this->testTimes as $i => $type) {
           foreach ($type as $j => $serializer) {
               $this->testTimes[$i][$j] = $serializer / $steps;
           }
        }

        print_r(json_encode($this->testTimes));
    }

    function getRandomPersonName()
    {
        $randomPersonName = ['John', 'Jane', 'Jack', 'Jill', 'Jenny', 'Jen', 'Jesse', 'J'];
        return $randomPersonName[array_rand($randomPersonName)];
    }

    function getRandomDepartment()
    {
        $randomDepartmentName = ['Engineering', 'Math', 'English', 'History', 'Art', 'Music', 'Computer Science'];
        return $randomDepartmentName[array_rand($randomDepartmentName)];
    }

    function getRandomLesson()
    {
        $randomLessonName = ['Math', 'Science', 'English', 'History', 'Art', 'Music', 'PE', 'Computer Science'];
        return $randomLessonName[array_rand($randomLessonName)];
    }

    function test()
    {

        $school = new School();

        for ($i = 0; $i < rand(100, 1000); $i++) {
            $student = new Student();
            $student->name = $this->getRandomPersonName();

            $department = new Department();
            $department->id = $i;
            $department->name = $this->getRandomDepartment();

            $school->departments[] = $department;

            $student->department = $department;
            $student->age = rand(18, 30);

            for ($j = 0; $j < rand(1, 5); $j++) {
                $lesson = new Lesson();
                $lesson->id = $j;
                $lesson->name = $this->getRandomLesson();
                $student->lessons[] = $lesson;
                $school->lessons[] = $lesson;
            }
            $school->students[] = $student;
        }

        for ($i = 0; $i < rand(100, 1000); $i++) {
            $teacher = new Teacher();
            $teacher->id = $i;
            $teacher->name = $this->getRandomPersonName();
            $teacher->salary = rand(10000, 100000);
            $teacher->age = rand(30, 60);

            $department = new Department();
            $department->id = $i;
            $department->name = $this->getRandomDepartment();

            $school->departments[] = $department;
            $teacher->department = $department;

            for ($j = 0; $j < rand(1, 5); $j++) {
                $lesson = new Lesson();
                $lesson->id = $j;
                $lesson->name = $this->getRandomLesson();
                $teacher->lessons[] = $lesson;
                $school->lessons[] = $lesson;
            }

            $school->teachers[] = $teacher;
        }

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);

        $time_start = microtime(true);
        $encodeSchool = $serializer->serialize($school, 'json');
        $time_end = microtime(true);
        $this->testTimes["encode"]["symfony"] += $time_end - $time_start;

        $time_start = microtime(true);
        $decodedSchool = $serializer->deserialize($encodeSchool, School::class, 'json');
        $time_end = microtime(true);
        $this->testTimes["decode"]["symfony"] += $time_end - $time_start;

        $serializer = SerializerBuilder::create()->build();

        $time_start = microtime(true);
        $encodeSchool = $serializer->serialize($school, 'json');
        $time_end = microtime(true);
        $this->testTimes["encode"]["jms"] = +$time_end - $time_start;

        $time_start = microtime(true);
        $decodedSchool = $serializer->deserialize($encodeSchool, School::class, 'json');
        $time_end = microtime(true);
        $this->testTimes["decode"]["jms"] = +$time_end - $time_start;

        $time_start = microtime(true);
        $encodeSchool = json_encode($school);
        $time_end = microtime(true);
        $this->testTimes["encode"]["php"] = +$time_end - $time_start;

        $time_start = microtime(true);
        $decodedSchool = json_decode($encodeSchool);
        $time_end = microtime(true);
        $this->testTimes["decode"]["php"] = +$time_end - $time_start;

    }


}

$test = new Index();

$test->run();


